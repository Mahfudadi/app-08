package saputro.mahfud.app08

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class Setting : AppCompatActivity(), View.OnClickListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    var BG_TITLE_COLOR = "bg_title_color"
    var FONT_TITLE_COLOR = "font_title_color"
    val FONT_TITLE_SIZE = "font_title_size"
    val TITLE_TEXT = "title_teks"
    val DEF_FONT_TITLE_SIZE = 24
    val DEF_TITLE_TEXT = "WEST WORLD"
    val FONT_DESC_SIZE = "font_desc_size"
    val DEF_TITLE_COLOR = "BLACK"
    val DEF_FONT_TITLE_COLOR  = "WHITE"
    val DESC_TEXT = "desc_teks"
    val DEF_FONT_DESC_SIZE = 12
    val DEF_DESC_TEXT = "Tontonan tahun ini"
    var FontTitleColor: String = ""
    var BGTitleColor: String = ""
    val arrayBGTitleColor = arrayOf("BLUE", "YELLOW", "GREEN", "RED")
    lateinit var adapterSpin: ArrayAdapter<String>

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(BG_TITLE_COLOR, BGTitleColor)
        prefEditor.putString(FONT_TITLE_COLOR,FontTitleColor)
        prefEditor.putInt(FONT_TITLE_SIZE, sbTitle.progress)
        prefEditor.putString(TITLE_TEXT, edTitle.text.toString())
        prefEditor.putInt(FONT_DESC_SIZE, sbDetail.progress)
        prefEditor.putString(DESC_TEXT, edDetail.text.toString())
        prefEditor.commit()
        Toast.makeText(this, "Setting Disimpan", Toast.LENGTH_SHORT).show()
    }

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            when (seekBar?.id) {
                R.id.sbTitle -> {
                    edTitle.setTextSize(progress.toFloat())
                }
                R.id.sbDetail -> {
                    edDetail.setTextSize(progress.toFloat())
                }
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        rgBGWarna.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbBiru -> FontTitleColor = "BLUE"
                R.id.rbKuning -> FontTitleColor = "YELLOW"
                R.id.rbHijau -> FontTitleColor = "GREEN"
                R.id.rbHitam -> FontTitleColor = "BLACK"
            }
        }

        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayBGTitleColor)
        spWarna.adapter = adapterSpin
        spWarna.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                BGTitleColor = adapterSpin.getItem(position).toString()
            }
        }

        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        var spinnerselection =
            adapterSpin.getPosition(preferences.getString(BG_TITLE_COLOR, DEF_TITLE_COLOR))
        spWarna.setSelection(spinnerselection, true)
        var rgselection = preferences.getString(FONT_TITLE_COLOR, DEF_FONT_TITLE_COLOR)
        if (rgselection == "BLUE") {
            rgBGWarna.check(R.id.rbBiru)
        } else if (rgselection == "YELLOW") {
            rgBGWarna.check(R.id.rbKuning)
        } else if (rgselection == "GREEN")  {
            rgBGWarna.check(R.id.rbHijau)
        } else {
            rgBGWarna.check(R.id.rbHitam)
        }

        edTitle.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        edTitle.textSize = preferences.getInt(FONT_TITLE_SIZE,DEF_FONT_TITLE_SIZE).toFloat()
        sbTitle.progress = preferences.getInt(FONT_TITLE_SIZE,DEF_FONT_TITLE_SIZE)
        sbTitle.setOnSeekBarChangeListener(onSeek)
        edDetail.setText(preferences.getString(DESC_TEXT,DEF_DESC_TEXT))
        edDetail.textSize = preferences.getInt(FONT_DESC_SIZE,DEF_FONT_DESC_SIZE).toFloat()
        sbDetail.progress = preferences.getInt(FONT_DESC_SIZE,DEF_FONT_DESC_SIZE)
        sbDetail.setOnSeekBarChangeListener(onSeek)

        btSimpan.setOnClickListener(this)
    }
}